#!/usr/bin/env python3
"""Functions for converting between times and strings, as well as the
   points calculation function."""

import math


def points(winnertime, time):
    """Get the achieved number of points for a race."""
    if time <= 0:
        return 0
    if winnertime <= time:
        return 100 - 50 * math.log(winnertime / time, 0.5)
    else:
        return time / winnertime * 100


def convert_time(time):
    """Convert a time string into numerical seconds."""
    if type(time) != str:
        return time
    parts = time.split(":")[::-1]
    total = 0
    b = 1
    for i in parts:
        # eval('01') fails:
        while i[0] == "0" and len(i) > 1:
            i = i[1:]

        total += eval(i) * b
        b *= 60
    return total


def time_format(time):
    """Format a number of seconds as a time string."""
    if type(time) == int:
        return str(time) + " points"
    secs = int(convert_time(time))
    if secs < 6000:
        return f"{secs // 60}:{str(secs % 60).zfill(2)}"
    else:
        hours = secs // 3600
        secs = secs % 3600
        return f"{hours}:{str(secs // 60).zfill(2)}:{str(secs % 60).zfill(2)}"
