#!/usr/bin/env python3
import os
import year

YEAR = year.YEAR
MINIWIDTH = 800

MFHEAD = f'<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="charset=utf-8"><link rel="icon" href="../icon.png"><title>PFR Club Championship</title><link rel="stylesheet" href="../style.css"></head><body><div class="maindiv"><img class="logo" src="../logo.png" alt="Penistone Footpath Runners & AC"><div class="title"><h1>Penistone Footpath Runners &amp; AC</h1><h2>{YEAR} Club Championship</h2></div><br><br><table class="minibox" style="width:{MINIWIDTH}px"><tr><th colspan="2">Overall</th><th colspan="2">Road</th><th colspan="2">Cross Country</th><th colspan="2">Fell</th><th colspan="2" rowspan="2"><a href="Challenge.html">Challenge</a></th><th colspan="2" rowspan="2"><a href="http://pfrac.co.uk/club-competitions/club-championship/" target="_blank">Information</a></th><th colspan="2" rowspan="2"><a href="calculator.html">Points Calculator</th></tr><tr><th><a href="Overall Female.html">F</a></th><th><a href="Overall Male.html">M</a></th><th><a href="Road Female.html">F</a></th><th><a href="Road Male.html">M</a></th><th><a href="Cross Country Female.html">F</a></th><th><a href="Cross Country Male.html">M</a></th><th><a href="Fell Female.html">F</a></th><th><a href="Fell Male.html">M</a></th></tr></table>'

FHEAD = f'<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="charset=utf-8"><link rel="icon" href="../icon.png"><title>PFR Fell League</title><link rel="stylesheet" href="../style.css"></head><body><div class="maindiv"><img class="logo" src="../logo.png" alt="Penistone Footpath Runners & AC"><div class="title"><h1>Penistone Footpath Runners &amp; AC</h1><h2>{YEAR} Fell League</h2></div><br><br><table class="minibox" style="width:{MINIWIDTH}px"><tr><th><a href="Fell League Female.html">Female</a></th><th><a href="Fell League Male.html">Male</a></th><th><a href="Races.html">Races</a></th><th><a href="http://pfrac.co.uk/club-competitions/fell-league/" target="_blank">Information</a></th><th><a href="calculator.html">Points Calculator</th></tr></table>'

NUMBERS = "0123456789"


def create_html_table(
    data,
    headingrow=0,
    columnalign=None,
    link=True,
    highlight=False,
    path="html/",
    linkables=[],
    numberlink=False,
    rotateheads=[],
    tableinfo="",
    sticky=True,
    bigbold=False,
):
    """Creates an HTML table of some data"""
    if columnalign is None:
        columnalign = ["text-align:left"]
    while len(columnalign) < len(data[0]):
        columnalign.append("text-align:center")

    # Determine how many padding (wierd) spaces are needed to fit all
    # of the numbers
    digitsneeded = []
    for i in range(0, len(data[0]), 1):
        digitsneeded.append(
            max(
                len(str(round(x[i]))) if isinstance(x[i], (float, int)) else 0
                for x in data
            )
        )
    table = f"<table {tableinfo}>"
    for i, row in enumerate(data):
        table += "<tr>"
        for j, cell in enumerate(row):
            text = str(cell)

            # Make hover text for floats
            if isinstance(cell, float):
                # Next line contains unicode funny space
                text = f'<span title="{round(cell, 5)}">{str(round(cell)).rjust(digitsneeded[j], " ")}</span>'
            elif isinstance(cell, int):
                text = text.rjust(digitsneeded[j], " ")

            # Internal hover text
            elif "." in text:
                startofnum = text.index(".")
                while text[startofnum - 1] in NUMBERS:
                    startofnum -= 1
                endofnum = text.index(".")
                while text[endofnum + 1] in NUMBERS:
                    endofnum += 1

                number = text[startofnum : endofnum + 1]
                text = f'<span title="{round(float(number), 6)}">{text.replace(number, str(round(float(number))))}</span>'

            # Make it a link if needed
            if link and (os.path.exists(path + text + ".html") or text in linkables):
                text = f'<a href="{text}.html">{text}</a>'

            # Number links
            if (
                numberlink
                and any((x in text for x in NUMBERS))
                and (numberlink[0] + text).replace(numberlink[1], "") in linkables
            ):
                text = f'<a href="{numberlink[0]} {"".join([x if x in NUMBERS else "" for x in text])}.html">{text}</a>'

            # Create boldness
            if highlight:
                if row in highlight:
                    text = f"<strong>{text}</strong>"

            # Make the largest number bold
            if isinstance(cell, float) and bigbold:
                if (
                    sorted(filter(lambda x: isinstance(x, float), row), reverse=True)[0]
                    == cell
                ):
                    text = f"<strong>{text}</strong>"

            # Do rotated text
            ##            if j in rotateheads and i == 0:
            ##                text = f'<div style="transform: rotate(-90deg);border-collapse:separate;height:150px;table-layout:fixed">{text}</div>'

            # Create if heading
            if i == headingrow:
                if sticky:
                    table += f'<th class="sticky" style="{columnalign[j]}">{text}</th>'
                else:
                    table += f'<th style="{columnalign[j]}">{text}</th>'

            # Empty cell optimisation
            elif text == "":
                table += "<td></td>"

            # Create standard cell
            else:
                table += f'<td style="{columnalign[j]}">{text}</td>'

        table += "</tr>"

    table += "</table>"
    table = table.replace(' style="text-align:center"', "")
    return table


def create_html_page(
    pagename,
    data,
    headingrow=0,
    columnalign=None,
    link=True,
    highlight=False,
    path="html/",
    linkables=[],
    header="",
    numberlink=False,
    rotateheads=[],
    fellhead=False,
    bigbold=False,
    runner=False,
):
    """Create an HTML page for some data"""
    if fellhead:
        h = FHEAD
    else:
        h = MFHEAD
    button = ""
    if runner:
        button = f'<table class="minibox" style="width:{MINIWIDTH}px"><th><a href="../Runner Pages/{pagename}.html">Click to see results for all of {pagename.split(" ")[0]}\'s competitions.</a></th></table>'
    head = f"{h}<h2>{pagename}</h2>{button}<p>Note: points are presented as integers. Hover over them to see exact values.</p>"
    if runner and not fellhead:
        head += "<p>⋄ indicates qualified (by doing sufficient races)</p>"
    head += f"{header}<br>"
    return f'{head}<div class="tablediv">{create_html_table(data, headingrow, columnalign, link, highlight, path=path, linkables=linkables, numberlink=numberlink, rotateheads=rotateheads, bigbold=bigbold)}</div>'
