#!/usr/bin/env python3
import shutil
import os
import fileio
import html
import runner
import points
import language
import year

YEAR = year.YEAR

PATHTOHTML = f"Championship{YEAR}/"
RACECATS = ("Overall", "Road", "Cross Country", "Fell", "Challenge")
RACEHEADS = ["Name", "Position", "Time", "Category", "Points"]
RUNNERHEADS = ["Race", "Category", "Date", "Position", "Time", "Points"]
MAINHEADS = [
    "Name",
    "Category",
    "Overall Points",
    "Road Points",
    "Cross Country Points",
    "Fell Points",
    "Challenge Points",
    "Races",
    "Races",
    "Races",
    "Races",
]

CATEGORIES = tuple(["U20", " Senior"] + [f"V{age}" for age in range(35, 85, 5)])

# Create a list of strings that can be hyperlinks
LINKEDTHINGS = [z[:-4] for z in fileio.list_races()] + list(fileio.runners.keys())
SERIESALIGN = ["text-align:left", "text-align:left"] + ["text-align:center"] * 10

RACEALIGN = [
    "text-align:left",
    "text-align:center",
    "text-align:center",
    "text-align:left;padding-left:40px",
    "text-align:center",
]

PAGENAMES = {
    "Road": "Road Section",
    "Cross Country": "Cross Country Section",
    "Fell": "Fell Section",
    "Overall": "Overall Championship",
    "Challenge": "Challenge Championship",
}

PERSONALIGN = [
    "text-align:left",
    "text-align:left;padding-left:20px",
    "text-align:center",
    "text-align:center",
    "text-align:center",
    "text-align:center",
]


class IllegalRunner(Exception):
    pass


def remove_columns(data, columns):
    """Remove the item at columns from each list in the list"""
    newdata = []
    for row in data:
        newrow = []
        for i, cell in enumerate(row):
            if i not in columns:
                newrow.append(cell)
        newdata.append(newrow)
    return newdata


def create_main(
    pagename, removecolumns, table, linkables, columnalign, headings, highlight
):
    """Create a main-type table"""
    newtable = [["Position"] + headings] + table

    newtable = remove_columns(newtable, removecolumns)
    highlight = remove_columns(highlight, removecolumns)
    return html.create_html_page(
        pagename,
        newtable,
        linkables=linkables,
        rotateheads=list(range(3, 50)),
        columnalign=columnalign,
        highlight=highlight,
    )


def indexbyindex(array, index, value):
    return next(i for i, v in enumerate(array) if v[index] == value)


def movetohtml(filename):
    shutil.copy2(filename, PATHTOHTML + filename)


def main():
    global LINKEDTHINGS

    # Create the directory containing the results
    if not os.path.exists(PATHTOHTML):
        os.makedirs(PATHTOHTML)

    # Put the calculator script into that directory
    movetohtml("calculator.js")

    # Get the races
    races = fileio.get_races("Championship")
    races.sort(key=lambda x: "".join(x.date.split("/")[::-1]))

    # Create the eventnames list
    eventnames = []
    catraces = {"Road": [], "Cross Country": [], "Fell": []}

    serieses = set()
    for race in races:
        if "Series" in race.category:
            rn = race.name[:-2]
            if rn not in eventnames:
                eventnames.append(rn)  # Breaks if >9 races
            catraces[race.category[:-7]].append(rn)
            serieses.add(rn)
        else:
            eventnames.append(race.name)
            try:
                catraces[race.category].append(race.name)
            except KeyError:
                # Race is not part of a normal category. This is fine.
                continue

    LINKEDTHINGS += list(serieses)

    # Calculate the best winning times for each series
    seriestimesM = {}
    seriestimesF = {}

    for series in serieses:
        for race in races:
            if series in race.name:
                wintimeM = points.convert_time(race.winningtimes["M"])
                wintimeF = points.convert_time(race.winningtimes["F"])

                if series in seriestimesM.keys():
                    seriestimesM[series] = min(wintimeM, seriestimesM[series])
                else:
                    seriestimesM[series] = wintimeM

                if series in seriestimesF.keys():
                    seriestimesF[series] = min(wintimeF, seriestimesF[series])
                else:
                    seriestimesF[series] = wintimeF

    for series in serieses:
        for race in races:
            if series in race.name:
                race.winningtimes["M"] = seriestimesM[series]
                race.winningtimes["F"] = seriestimesF[series]
                race.calculate_points()

    # Create the race files
    for r in races:
        header = (
            html.create_html_table(r.winnertable, headingrow=None) + "<p></p>"
        ) + f"<em>{len(r.results)} Penistone runners</em><p></p>"
        with open(f"{PATHTOHTML}{r.name}.html", "w") as f:
            f.write(
                html.create_html_page(
                    r.name,
                    [RACEHEADS] + r.results,
                    header=header,
                    linkables=LINKEDTHINGS,
                    columnalign=RACEALIGN,
                )
            )

    # Create some info for the per-person files
    people = []
    for runnername in fileio.runners.keys():
        cat = fileio.runners[runnername][1]
        # Ignore under categories for championship
        if "U" in cat:
            fileio.runners[runnername][1] = f"{cat[0]}U20"
        person = runner.Runner(
            runnername, fileio.runners[runnername][0], fileio.runners[runnername][1]
        )
        for r in races:
            for run in r.results:
                if run[0] == runnername:
                    person.add_result(
                        [r.name] + [r.category] + [r.date] + run[1:3] + [run[4]]
                    )

        if person.results:
            if fileio.runners[runnername][2] != "TRUE":
                raise IllegalRunner
            people.append(person)

    # Make main tables
    maintable = []
    counterlists = {
        "Challenge": [],
        "Road": [],
        "Cross Country": [],
        "Fell": [],
        "Overall": [],
    }

    for p in people:
        row = [
            p.name,
            p.category,
            p.overallpoints,
            p.points["Road"],
            p.points["Cross Country"],
            p.points["Fell"],
            p.challengepoints,
            p.totalraces,
            p.racecounts["Road"],
            p.racecounts["Cross Country"],
            p.racecounts["Fell"],
        ]
        for r in eventnames:
            for result in p.results:
                if r == result[0]:
                    row.append(result[5])
                    break
                if r in result[0]:
                    # [:-7] removes 'series'
                    result = p.get_series_score(result[1][:-7], r)
                    if result is None:
                        row.append("")
                    else:
                        row.append(result)
                    break
            else:
                row.append("")
        for category in PAGENAMES:
            if p.qualified[category]:
                counterlists[category].append(p.name)

        maintable.append(row)

    mainheads = MAINHEADS + eventnames

    tables = [
        ["Overall", [4, 5, 6, 7, 9, 10, 11], 5],
        [
            "Road",
            [3, 5, 6, 7, 8, 10, 11]
            + [
                None if r in catraces["Road"] else mainheads.index(r) + 1
                for r in eventnames
            ],
            None,
        ],
        [
            "Cross Country",
            [3, 4, 6, 7, 8, 9, 11]
            + [
                None if r in catraces["Cross Country"] else mainheads.index(r) + 1
                for r in eventnames
            ],
            None,
        ],
        [
            "Fell",
            [3, 4, 5, 7, 8, 9, 10]
            + [
                None if r in catraces["Fell"] else mainheads.index(r) + 1
                for r in eventnames
            ],
            None,
        ],
        ["Challenge", [2, 3, 4, 5, 6, 9, 10, 11], 4],
    ]

    # Define the main table alignments
    mainalign = ["text-align:left", "text-align:left", "text-align:left"]
    challengealign = mainalign[1:]

    # Create the actual main tables
    for j, i in enumerate(tables):
        ntd = list(filter(lambda x: x[2 + j], maintable))
        if i[0] == "Challenge":
            nts = [["", sorted(ntd, key=lambda x: x[6], reverse=True)]]
            align = challengealign
        else:
            align = mainalign
            nts = []
            for gender in ("Male", "Female"):
                # Sort and filter the table
                nts.append(
                    [
                        f" {gender}",
                        sorted(
                            filter(lambda x: x[1][0] == gender[0], ntd),
                            key=lambda x: x[2 + j],
                            reverse=True,
                        ),
                    ]
                )

        for g, nt in nts:
            # Find qualifiers
            nt.sort(key=lambda x: x[0] in counterlists[i[0]], reverse=True)
            # Add the position column
            nt = [[x + 1] + nt[x] for x in range(0, len(nt))]
            # Re-make, with positions
            qualified = list(filter(lambda x: x[1] in counterlists[i[0]], nt))

            pagename = f"{PAGENAMES[i[0]]} – {g}".strip(" – ").replace("  ", " ")
            with open(f"{PATHTOHTML}{i[0]}{g}.html", "w") as f:
                # Create the table
                p = create_main(
                    pagename, i[1], nt, LINKEDTHINGS, align, mainheads, qualified
                )
                # Write it
                f.write(p)

    # Calculate the positions and make the runner files

    # Open the cross-results site individual pages
    with open("../RunnerPages/pages.txt", "r") as f:
        pages = eval(f.read())

    for person in people:
        runnername = person.name
        cat = person.category
        minitable = []
        for i, section in enumerate(RACECATS):
            row = [section]

            if section == "Overall":
                score = language.pluralise(person.overallpoints, "point", "points")
            elif section == "Challenge":
                score = language.pluralise(person.challengepoints, "point", "points")
            else:
                score = language.pluralise(person.points[section], "point", "points")

            if score == "0 points":
                row += ["", "", score]
                minitable.append(row)
                continue

            sortedtable = sorted(
                maintable,
                key=lambda x: (x[0] in counterlists[section], x[i + 2]),
                reverse=True,
            )

            if section == "Challenge":
                genderfilter = sortedtable
            else:
                genderfilter = filter(lambda x: x[1][0] == cat[0], sortedtable)
            catfilter = filter(lambda x: x[1] == cat, sortedtable)

            # Position
            pos = language.numberpositions(
                1 + indexbyindex(genderfilter, 0, runnername)
            )
            row.append(f"{pos} overall")
            if runnername in counterlists[section]:
                row[-1] += " ⋄"

            # Age category position
            agepos = language.numberpositions(
                1 + indexbyindex(catfilter, 0, runnername)
            )
            row.append(f"{agepos} {cat}")
            if runnername in counterlists[section]:
                row[-1] += " ⋄"

            row.append(score)

            minitable.append(row)

        minitable = list(map(list, zip(*minitable)))
        header = html.create_html_table(
            minitable,
            tableinfo='style="100%;table-layout:fixed"',
            columnalign=["text-align:center"],
            sticky=False,
        )

        # Cross-site
        if person.name not in pages:
            pages[person.name] = {}
        pages[person.name]["Club Championship"] = header

        with open(f"{PATHTOHTML}{runnername}.html", "w") as f:
            f.write(
                html.create_html_page(
                    runnername,
                    [RUNNERHEADS] + person.results,
                    linkables=LINKEDTHINGS,
                    header=header,
                    highlight=person.counters,
                    columnalign=PERSONALIGN,
                    runner=True,
                )
            )

    with open("../RunnerPages/pages.txt", "w") as f:
        f.write(str(pages))

    # Create the series tables
    for series in serieses:
        cat = {
            "Spencers Dash": "Road",
            "Huddersfield 5K": "Road",
            "Silkstone Shuffle": "Cross Country",
            "Trunce": "Fell",
            "Furty Furlong": "Cross Country",
        }[series]

        # Find how many there are in the series
        currentmax = 0
        for r in races:
            if series in r.name:
                number = int(r.name.replace(series + " ", ""))
                if number > currentmax:
                    currentmax = number

        # Build the table
        table = []
        for p in sorted(
            filter(lambda x: x.get_series_score(cat, series) is not None, people),
            key=lambda x: x.get_series_score(cat, series),
            reverse=True,
        ):
            row = [p.name, p.category]
            for i in range(1, currentmax + 1):
                for result in p.results:
                    if series in result[0]:
                        if int(result[0].replace(series + " ", "")) == i:
                            row.append(result[5])
                            break
                else:
                    row.append("")
            table.append(row)

        with open(f"{PATHTOHTML}{series}.html", "w") as f:
            f.write(
                html.create_html_page(
                    series,
                    [
                        ["Name", "Category"]
                        + ["Race " + str(x) for x in range(1, currentmax + 1)]
                    ]
                    + table,
                    linkables=LINKEDTHINGS,
                    numberlink=[series, "Race"],
                    columnalign=SERIESALIGN,
                    bigbold=True,
                )
            )

    # Create the points calculator page
    with open("calccode.html", "r") as f:
        calccode = f.read()
    with open(f"{PATHTOHTML}calculator.html", "w") as f:
        f.write(html.MFHEAD + calccode)

    # Make the prizes page
    prizedata = []
    for section in ("Road", "Cross Country", "Fell"):
        for gender in "FM":
            for age_cat in CATEGORIES:
                qualified = list(
                    filter(
                        lambda r: r.category == gender + age_cat
                        and r.qualified[section],
                        people,
                    )
                )
                ranked = sorted(
                    qualified, key=lambda r: r.points[section], reverse=True
                )
                formatted = [
                    [section, gender + age_cat, i + 1, r.name, r.points[section]]
                    for i, r in enumerate(ranked)
                ]
                prizedata += formatted[:2]

    headings = ["Section", "Category", "Position", "Name", "Points"]
    with open(f"{PATHTOHTML}prizes.html", "w") as f:
        f.write(
            html.create_html_page(
                "Prizes", [headings] + prizedata, linkables=LINKEDTHINGS
            )
        )


if __name__ == "__main__":
    main()
