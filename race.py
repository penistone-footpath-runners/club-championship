#!/usr/bin/env python3
import points
import fileio


class BadRunner(Exception):
    pass


class Race:
    def __init__(self, name, date, category, winners=None, results=None):
        self.pointsdone = False
        self.name = name
        self.date = date
        self.category = category

        # Deal with races with no male or female winner
        oldwins = winners[:]
        winners = []
        for winner in oldwins:
            if winner not in (["F"], ["M"]):
                winners.append(winner)

        self.winningtimes = {i[1]: points.convert_time(i[2]) for i in winners}
        self.winnertable = winners
        self.results = results
        self.add_genders()
        self.calculate_points()
        self.format_times()
        # Format the winners' times
        for w in self.winnertable:
            w[2] = points.time_format(w[2])

    def add_genders(self):
        """Add the genders and filter the bad runners out"""
        newresults = []
        for i in range(0, len(self.results), 1):
            if "Fell" in self.category:
                allowedid = 3
            else:
                allowedid = 2

            if (
                self.results[i][0] in fileio.runners.keys()
                and fileio.runners[self.results[i][0]][allowedid] == "TRUE"
            ):
                newresults.append(
                    self.results[i] + [fileio.runners[self.results[i][0]][1]]
                )
            else:
                raise BadRunner(
                    f"Invalid runner: {self.results[i][0]} (Race is {self.name})"
                )
        self.results = newresults

    def calculate_points(self):
        """Calculate points for everyone"""
        if self.pointsdone:
            self.results = [
                x[:-1]
                + [points.points(self.winningtimes[x[3][0]], points.convert_time(x[2]))]
                for x in self.results
            ]
        else:
            self.results = [
                x
                + [points.points(self.winningtimes[x[3][0]], points.convert_time(x[2]))]
                for x in self.results
            ]
        self.pointsdone = True

    def format_times(self):
        for r in self.results:
            r[2] = points.time_format(r[2])
