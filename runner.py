#!/usr/bin/env python3
class Runner:
    """A runner for the Championship"""

    def __init__(self, name, gender, age_cat):
        self.name = name
        self.gender = gender
        self.category = age_cat
        self.results = []
        self.categories = ("Road", "Cross Country", "Fell")

    def add_result(self, result):
        """Add a result to the runner"""
        self.results.append(result)
        self.calculate_points_and_runs()

    def calculate_points_and_runs(self):
        """Calculate all of the points and best results"""
        self.points = {}
        self.racecounts = {}
        self.counters = []
        self.qualified = {
            "Challenge": True,
            "Overall": False,
            "Road": False,
            "Cross Country": False,
            "Fell": False,
        }
        allcounters = []
        reducers = 0
        self.seriesscores = {"Road": {}, "Cross Country": {}, "Fell": {}}
        for cat in self.categories:
            serieses = {}
            runs = []
            for run in self.results:
                if cat == run[1]:
                    runs.append(run)
                elif cat in run[1]:
                    # Assign the result to the correct series
                    if run[0][:-2] in serieses:
                        serieses[run[0][:-2]].append(run)
                    else:
                        serieses[run[0][:-2]] = [run]

            # Find the best in the series
            for name, series in serieses.items():
                if series:
                    series.sort(key=lambda x: x[5], reverse=True)
                    runs.append(series[0])
                    self.seriesscores[cat][name] = series[0][5]
                else:
                    self.seriesscores[cat][name] = None

            self.racecounts[cat] = len(runs)
            # Find the best two and three
            runs.sort(key=lambda x: x[5], reverse=True)
            reducers += max((0, 2 - len(runs)))
            best2 = runs[: min((2, len(runs)))]
            best3 = runs[: min((3, len(runs)))]

            self.counters += best2

            # Store the individual category points
            if len(best3) == 3:
                self.qualified[cat] = True
            self.points[cat] = sum(x[5] for x in best3)

            allcounters += runs

        for run in self.results:
            if run[1] == "Club mile":
                allcounters.append(run)
        self.challengepoints = sum(x[5] for x in allcounters)

        # Bring the counters up to 9
        allcounters.sort(key=lambda x: x[5], reverse=True)
        for run in allcounters:
            if len(self.counters) >= (9 - reducers):
                break
            if run not in self.counters:
                self.counters.append(run)
        # Remove excess runs
        while len(self.counters) > 9:
            self.counters.remove(min(self.counters, key=lambda x: x[5]))

        self.overallpoints = sum(x[5] for x in self.counters)
        self.totalraces = sum(self.racecounts.values())
        # Add one to totalraces to account for club mile
        for run in self.results:
            if run[1] == "Club mile":
                self.totalraces += 1

        if len(self.counters) == 9:
            self.qualified["Overall"] = True

    def get_series_score(self, category, series):
        try:
            return self.seriesscores[category][series]
        except KeyError:
            return None

    def __repr__(self):
        return f"Runner({self.name!r}, {self.gender!r}, {self.category!r})"
