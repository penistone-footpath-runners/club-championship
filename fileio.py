#!/usr/bin/env python3
import os
import race
import copy


def UKtoISO(date):
    """Covert UK time to ISO"""
    return "-".join(date.split("/")[::-1])


def extract_table(text):
    """Extract an table from a string version"""
    table = [
        [
            "" if not y else eval(y) if y[0] in "0123456789" and ":" not in y else y
            for y in x.split(",")
        ]
        for x in text.split("\n")
    ]
    while table[-1] == [""]:
        table = table[:-1]
    return table


def get_races(allowed):
    """Find all of the race data"""
    racefiles = ["races/" + x for x in list_races()]
    races = []
    for r in racefiles:
        with open(r, "r") as f:
            racetext = f.read()
        findifallowed = racetext.split("\n")
        good = False
        for i in findifallowed:
            if allowed in i and "TRUE" in i:
                good = True
        if not good:
            continue

        racetext = racetext.split("\n")[2:]
        # Remove commas on the ends of lines
        racetext = "\n".join([x.strip(",") for x in racetext])

        metadata, results = racetext.split("\n\n")
        metadata = metadata.split("\n")
        name, date, category = metadata[:3]
        winners = extract_table("\n".join(metadata[3:]))

        results = extract_table(results)

        races.append(race.Race(name, date, category, winners, copy.deepcopy(results)))
    return races


def list_races():
    """List all of the races in the races/ directory"""
    return [x for x in os.listdir("races/")]


def get_runners():
    """Find the list of runners"""
    with open("Members.csv", "r") as f:
        members = {
            x[1]: x[2:] for x in filter(lambda x: any(x[2:]), extract_table(f.read()))
        }
    return members


runners = get_runners()
