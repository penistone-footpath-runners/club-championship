#!/usr/bin/env python3
import os
import shutil

from fractions import Fraction

import fileio
import html
import fellrunner as runner
import language
import year
import predictions

YEAR = year.YEAR

PATHTOHTML = f"FellLeague{YEAR}/"
RACEHEADS = ["Name", "Position", "Time", "Category", "Points"]
RUNNERHEADS = ["Race", "Date", "Position", "Time", "Points"]
CATEGORIES = (
    "MU11",
    "MU13",
    "MU15",
    "MU17",
    "MU20",
    "M Senior",
    "MV35",
    "MV40",
    "MV45",
    "MV50",
    "MV55",
    "MV60",
    "MV65",
    "MV70",
    "MV75",
    "MV80",
    "MV85",
    "MV90",
    "MV95",
    "FU11",
    "FU13",
    "FU15",
    "FU17",
    "FU20",
    "F Senior",
    "FV35",
    "FV40",
    "FV45",
    "FV50",
    "FV55",
    "FV60",
    "FV65",
    "FV70",
    "FV75",
    "FV80",
    "FV85",
    "FV90",
    "FV95",
)

ATONCE = 1000
LINKEDTHINGS = [x[:-4] for x in fileio.list_races()] + list(fileio.runners.keys())

RACEALIGN = [
    "text-align:left",
    "text-align:center",
    "text-align:center",
    "text-align:left;padding-left:40px",
    "text-align:center",
]

PERSONALIGN = [
    "text-align:left",
    "text-align:center",
    "text-align:center",
    "text-align:right",
    "text-align:center",
]

MAINALIGN = [
    "text-align:left",
    "text-align:left",
    "text-align:left",
    "text-align:center",
    "text-align:center",
]

COLNUM = 40


class IllegalRunner(Exception):
    pass


def remove_columns(data, columns):
    """Remove the item at columns from each list in the list"""
    newdata = []
    for row in data:
        newrow = []
        for i, cell in enumerate(row):
            if i not in columns:
                newrow.append(cell)
        newdata.append(newrow)
    return newdata


def create_main(pagename, removecolumns, table, linkables, columnalign, headings):
    """Create a main-type table"""
    newtable = remove_columns([["Position"] + headings] + table, removecolumns)
    return html.create_html_page(
        pagename,
        newtable,
        linkables=linkables,
        rotateheads=list(range(3, 50)),
        columnalign=columnalign,
        fellhead=True,
        path=PATHTOHTML,
    )


def probabilty_to_odds(p):
    if p == 0:
        return ""
    if p > 0.1:
        f = Fraction(p).limit_denominator(10)
        return f"{f.numerator}/{f.denominator - f.numerator}"
    return f"{round((1 / p) - 1)}/1"


def movetohtml(filename):
    shutil.copy2(filename, PATHTOHTML + filename)


def main():
    if not os.path.exists(PATHTOHTML):
        os.makedirs(PATHTOHTML)

    # Put the calculator script into that directory
    movetohtml("calculator.js")

    # Get the races
    races = fileio.get_races("Fell League")
    races.sort(key=lambda x: "".join(x.date.split("/")[::-1]))

    # Create the eventnames list
    eventnames = []

    for race in races:
        eventnames.append(race.name)

    MAINHEADS = ["Name", "Category", "Points", "Races"]

    # Create the race files
    for r in races:
        header = (
            html.create_html_table(r.winnertable, headingrow=None) + "<p></p>"
        ) + f"<em>{len(r.results)} Penistone runners</em><p></p>"
        if len(r.results) == 1:
            header = header.replace("runners", "runner")
        with open(f"{PATHTOHTML}{r.name}.html", "w") as f:
            f.write(
                html.create_html_page(
                    r.name,
                    [RACEHEADS] + r.results,
                    header=header,
                    linkables=LINKEDTHINGS,
                    columnalign=RACEALIGN,
                    fellhead=True,
                    path=PATHTOHTML,
                )
            )

    # Create the personal data for main table creation and runner pages
    people = []
    for runnername in fileio.runners.keys():
        person = runner.Runner(
            runnername, fileio.runners[runnername][0], fileio.runners[runnername][1]
        )
        for r in races:
            for run in r.results:
                if run[0] == runnername:
                    person.add_result([r.name] + [r.date] + run[1:3] + [run[4]])

        if person.results:
            people.append(person)
            if fileio.runners[runnername][3] != "TRUE":
                raise IllegalRunner

    # Make main tables
    maintable = []
    for p in people:
        row = [
            p.name,
            p.category,
            p.points,
            p.totalraces,
        ]

        maintable.append(row)

    # Create the actual main table
    nt = sorted(maintable, key=lambda x: x[2], reverse=True)
    gendertables = {z: list(filter(lambda x: x[1][0] == z, nt)) for z in ("M", "F")}
    ranks = {
        z: {b: a + 1 for a, b in enumerate([i[0] for i in gendertables[z]])}
        for z in ("M", "F")
    }

    # Category ranks
    catranks = {
        z: {
            b: a + 1
            for a, b in enumerate([i[0] for i in list(filter(lambda x: x[1] == z, nt))])
        }
        for z in CATEGORIES
    }

    # Open the cross-results site individual pages
    with open("../RunnerPages/pages.txt", "r") as f:
        pages = eval(f.read())

    # Create the runner pages
    for person in people:
        name = person.name
        # Position and points
        header = f'<div class="column"><em>{language.numberpositions(ranks[person.gender][name])} overall</em></div><div class="column"><em>{language.numberpositions(catranks[person.category][name])} {person.category}</em></div><div class="column"><span title={round(person.points, 6)}><em>{language.pluralise(round(person.points), "point", "points")}</em></div><br>'

        # Cross-site stuff
        if person.name not in pages:
            pages[person.name] = {}
        pages[person.name]["Fell League"] = header

        with open(f"{PATHTOHTML}{name}.html", "w") as f:
            f.write(
                html.create_html_page(
                    name,
                    [RUNNERHEADS] + person.results,
                    linkables=LINKEDTHINGS,
                    header=header,
                    columnalign=PERSONALIGN,
                    fellhead=True,
                    path=PATHTOHTML,
                    runner=True,
                )
            )

    # Write the cross-site results thing
    with open("../RunnerPages/pages.txt", "w") as f:
        f.write(str(pages))

    # Create the overall pages
    for g in ("Male", "Female"):
        ttw = gendertables[g[0]]
        # Add the position column
        ttw = [[x + 1] + ttw[x] for x in range(0, len(ttw))]

        pagename = f"Fell League – {g}"
        with open(f"{PATHTOHTML}Fell League {g}.html", "w") as f:
            # Write the table
            f.write(create_main(pagename, [], ttw, LINKEDTHINGS, MAINALIGN, MAINHEADS))

    racesindex = [[r.name, r.date, len(r.results)] for r in races]
    racesindex.sort(key=lambda x: (x[1].split("/")[::-1], x[0]))
    racesindex = [["Race", "Date", "Penistone Runners"]] + racesindex
    with open(f"{PATHTOHTML}Races.html", "w") as f:
        # Write the table
        f.write(
            html.create_html_page(
                "Races", racesindex, fellhead=True, linkables=LINKEDTHINGS
            )
        )

    # Create a data file with everyone's points history
    # (This is only used for making some nice graphs manually)
    # Make the list of dates that matter
    with open("days.txt", "r") as f:
        days = f.read().split("\n")
    filedata = [["Date"]]
    people.sort(key=lambda x: x.points, reverse=True)
    for person in people:
        filedata[0].append(person.name)

    # Make the individual rows
    for day in days:
        newrow = [day]
        for person in people:
            newrow.append(
                str(
                    sum(
                        [
                            x[4]
                            for x in filter(
                                lambda x: sorted(
                                    [x[1], day], key=lambda x: x.split("/")[::-1]
                                )
                                == [x[1], day],
                                person.results,
                            )
                        ]
                    )
                )
            )

        filedata.append(newrow)

    with open("history.csv", "w") as f:
        f.write("\n".join([",".join(x) for x in filedata]))

    # Create the points calculator page
    with open("calccode.html", "r") as f:
        calccode = f.read()
    with open(f"{PATHTOHTML}calculator.html", "w") as f:
        f.write(html.FHEAD + calccode)

    # Make predictions
    predrunners = {
        G: [
            predictions.Runner(
                r.name, r.get_points_only(), [0 for _ in range(COLNUM + 1)]
            )
            for r in filter(lambda p: p.gender == G, people)
        ]
        for G in ("F", "M")
    }
    n = 0
    scores = {}
    while True:
        n += 1
        for G in ("F", "M"):
            prs = predrunners[G]
            for pr in prs:
                scores[pr.name] = pr.simulate()
            fpr = sorted(prs, key=lambda pr: scores[pr.name], reverse=True)
            # print([(x.name, round(scores[x.name])) for x in fpr])
            for i, pr in enumerate(fpr[:40]):
                pr.add_rank(i + 1)
        if n % ATONCE:
            continue
        for G in ("F", "M"):
            data = [["Name"] + [str(i) for i in range(1, COLNUM)]]
            for rn in predrunners[G]:
                data.append(
                    [rn.name]
                    + [round(rn.ranks[r] / n * 100, 1) for r in range(1, COLNUM)]
                )
                ##data.append([rn.name] + [probabilty_to_odds(rn.ranks[r] / n) for r in range(1, COLNUM)])
                # print(data)
            if G == "M":
                title = f"Male Predictions ({n} iterations)"
            else:
                title = f"Female Predictions ({n} iterations)"
            pagetext = html.create_html_page(
                title, data, fellhead=True, linkables=LINKEDTHINGS
            )

            with open(f"{PATHTOHTML}predictions{G}.html", "w") as f:
                f.write(pagetext)
        if n == ATONCE:
            print("Done")


if __name__ == "__main__":
    main()
