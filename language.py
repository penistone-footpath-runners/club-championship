#!/usr/bin/env python3
def numberpositions(n):
    n = str(n)
    if n[-1] == "1":
        return n + "st"
    if n[-1] == "2":
        return n + "nd"
    if n[-1] == "3":
        return n + "rd"
    return n + "th"


def pluralise(n, word, plural):
    n = str(n)
    if n == "1":
        return f"{n} {word}"
    return f"{n} {plural}"
