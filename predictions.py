#!/usr/bin/env python3
import datetime
import numpy

TODAY = datetime.datetime.today().date()
DAYSGONE = (TODAY - datetime.date(datetime.datetime.today().year, 1, 1)).days
DAYSLEFT = (datetime.date(datetime.datetime.today().year, 12, 31) - TODAY).days


class Runner:
    """A runner, for the purposes of simulation"""

    def __init__(self, name, points, ranks=[0 for _ in range(9999)]):
        self.name = name
        self.mean = numpy.mean(points)
        if len(points) > 1:
            self.stdev = numpy.std(points, ddof=1)
        else:
            self.stdev = 0
        racesperday = len(points) / DAYSGONE
        self.racerate = racesperday * DAYSLEFT
        self.ranks = ranks.copy()
        self.points = points
        self.totalpoints = sum(self.points)

    def simulate(self):
        races = numpy.random.poisson(self.racerate)
        expected = self.mean * races
        return (
            numpy.random.normal(expected, self.stdev * numpy.sqrt(races))
            + self.totalpoints
        )

    def add_rank(self, rank):
        self.ranks[rank] += 1

    def __repr__(self):
        return f"Runner({self.name!r}, {self.points!r}, ranks={self.ranks!r})"
